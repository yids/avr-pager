#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "../libs/pagerlib/pagerlib.h"
void load_a_lot_of_keys(struct pl_ctx *ctx)
{
  char *pubkey;
  for(int i=1; i<6; i++){
    pubkey = NULL;
    char filename[5];
    sprintf(filename, "%d.pub",i);
    printf("loading pubkey: %s\n", filename); 
    pubkey = pl_load_public_key(filename);
    pl_load_public_key_in_list(ctx, pubkey, NULL);
  }
}

int main()
{
 // struct pl_keypair *tx_kp;
  struct pl_ctx *tx_ctx;
  char clear_message[] = "Blaat blaat, dlaat ";
  //char *rx_pubkey;

  /* init */
  //tx_kp = NULL;
  tx_ctx = pl_init();

  /* load keypair */
   pl_load_keypair_from_file(tx_ctx, "tx");

  /*load public key of receiver*/
//  char *pubkey;
//  pubkey = pl_load_public_key("rx.pub");
  char pubkey[crypto_box_PUBLICKEYBYTES] = {0xA9, 0xB4, 0xA1, 0xF3, 0x56, 0x27, 0x70, 0x9E, 0xBD, 0x32, 0x27, 0x0A, 0xB8, 0x06, 0xE7, 0xE8, 0x98, 0xA4, 0x11, 0xD8, 0xB5, 0x64, 0x84, 0x64, 0xB6, 0xB6, 0x48, 0xEC, 0x0C, 0xA4, 0x1F, 0x49};

 // pl_load_public_key_in_list(tx_ctx, pubkey,"test");

//  pl_print_public_keylist(tx_ctx);
  pl_set_receiver(tx_ctx, pubkey);
  
  memcpy(tx_ctx->msg->msg, clear_message, MSG_SIZE); // copy clear msg into context

  switch(pl_send_message(tx_ctx)) { // send msg
      case 0:
 //       printf("SEND: OK! \n");
        break;
      case -1:
        printf("SEND: -1 \n");
        break;
      case -2:
        printf("SEND: -2 \n");
        break;
      case -3:
        printf("SEND: -3 \n");
        break;
      default:
        printf("SEND: unknown error \n");
        break;
    }
  write(STDOUT_FILENO,tx_ctx->msg,  sizeof(struct pl_pagermessage)); // dump to stdout
 // pl_cleanup(tx_ctx);
 // free(pubkey);
 ////// RRRRXXXX /////////
  return 0;
}
