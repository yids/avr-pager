
int list_pubkeys();
int send_message();
int help();
int quit();

/* structure for the commands */
typedef struct{
  char *name;                   /* User printable name of the function. */
  rl_icpfunc_t *func;           /* Function to call to do the job. */
  char *doc;                    /* Documentation for this function.  */
} COMMAND;  

static COMMAND commands[] = {
  {"listPublicKeys", list_pubkeys, "Shows a list of the loaded public keys"},
  {"sendMessage", send_message, "Sends a message"},
  {"help", help, "Shows help"},
  {"quit", quit, "Exits pager-cli"}
};



