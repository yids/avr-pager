/*
 * test thingie for the pagerlib
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

#include "../libs/pagerlib/pagerlib.h"


int main() {
  struct pl_keypair *receiver, *sender;
  receiver = NULL;
  sender = NULL;
  char clear_message[] = "Blaat blaat, dit is een test berichtje :), en nog meer en meer en meer 123456744555 blablablablablablabal jajajajaj hee blaat ";

  // initialise the pagers
  struct pl_ctx *sender_ctx, *rcv_ctx, *context;

  rcv_ctx = pl_init();
  sender_ctx = pl_init();

  /* create keypairs */
  receiver = pl_create_keypair(rcv_ctx);
  pl_load_key_in_list(rcv_ctx, receiver);
  pl_load_key_in_list(sender_ctx, receiver);
  rcv_ctx->kp = receiver;

  sender = pl_create_keypair(sender_ctx);
  pl_load_key_in_list(sender_ctx, sender);
  pl_load_key_in_list(rcv_ctx, sender);
  sender_ctx->kp = sender;



  /* 
     char *kp_b64, *decoded;
  // 4*(n/3) 
  kp_b64 = malloc(4*(sizeof(struct pl_keypair) / 3));
  decoded = malloc(sizeof(struct pl_keypair));
  base64_encode(kp_b64, kp, sizeof(struct pl_keypair));
  printf("base64 keypair:  %s \n", kp_b64);
  base64_decode(decoded , kp_b64, (4*sizeof(struct pl_keypair) / 3)) ;
  */

  int  f = 0;
  while ( f < 10 ) {

    f++;
    // load the message to send
    memcpy(sender_ctx->msg->msg, clear_message, MSG_SIZE);

    //to who to send the message to (get from kp)
    memcpy(&sender_ctx->receiver_public_key, &receiver->public_key, sizeof(sender_ctx->receiver_public_key));
    int r;
    switch(pl_send_message(sender_ctx)) {
      case 0:
        printf("SEND: OK! \n");
        break;
      case -1:
        printf("SEND: -1 \n");
        break;
      case -2:
        printf("SEND: -2 \n");
        break;
      case -3:
        printf("SEND: -3 \n");
        break;
      default:
        printf("SEND: unknown error \n");
        break;

    }
    //    printf("crypted msg: %s",sender_ctx->msg->msg);

    /* copy the message to receiver */
    struct pl_pagermessage * message = NULL;
    message = (struct pl_pagermessage *) malloc(sizeof(struct pl_pagermessage));
    memcpy(message, sender_ctx->msg, sizeof( struct pl_pagermessage));
    rcv_ctx->msg = message;
    message = NULL;

    printf("\n encrypted message: \n %s \n END encrypted message \n", rcv_ctx->msg->msg );

    switch(pl_receive_message(rcv_ctx)) {
      case 0:
        printf("RCV: OK! \n");
        printf(" Message! \n to: %u  from: %u \n the decrypted message: %s \n", rcv_ctx->msg->address, compressed_point_to_addr(rcv_ctx->msg->sender_public_key), rcv_ctx->msg->msg);

        break;
      case -1:
        printf("RCV: -1 (box open error)\n");
        break;
      case -2:
        printf("RCV: -2 \n");
        break;
      case -3:
        printf("RCV: -3 (zerobytes not zero)\n");
        break;
      case -4:
        printf("RCV: -4 (key not found) \n");
        break;

      default:
        printf("RCV: unknown error \n");
        break;


    }  
  }
/*
   DBG("\n sender.keypair");
   DBM("sender", sizeof(struct pl_keypair), sender);
   DBM("receiver", sizeof(struct pl_keypair), receiver);
   DBM("context->kp (sender)", sizeof(struct pl_keypair), context->kp);
   DBM("context->kp->private_key (sender)", sizeof(context->kp->private_key), &context->kp->private_key);
   DBM("receiver->compressed",sizeof(receiver->compressed_point) , &receiver->compressed_point);
   DBM("context->receiver_compressed_point",sizeof(context->receiver_compressed_point) , &context->receiver_compressed_point);
   DBM("context->kp (receiver)", sizeof(struct pl_keypair), context->kp);
   DBM("context->kp->private_key (receiver)", sizeof(context->kp->private_key), &context->kp->private_key);

*/

context = rcv_ctx;


pl_inbox_display_all(context);


while (pl_inbox_next(context))
{
pl_inbox_display(context);
}

while (pl_inbox_prev(context)) {
pl_inbox_display(context);
}
while (pl_inbox_next(context) && context->inbox->prev != NULL && context->inbox->prev->id > 9)
{
pl_inbox_delete(context);
}

while (pl_inbox_prev(context)) {
pl_inbox_display(context);
}
//  pl_inbox_delete(context); 
pl_inbox_display(context);
return 0;

} // main
