#include <stdio.h>                                                                                                                                                                   
#include <string.h>
#include <stdlib.h>
#include <unistd.h>


#include <readline/readline.h>
#include <readline/history.h>
#include "../libs/pagerlib/pagerlib.h"
#include "pager-cli.h"

struct pl_ctx *ctx;

int list_pubkeys()
{
  pl_print_public_keylist(ctx);
  return 0;
}

int send_message()
{
  char *addr;
  char *msg;
  list_pubkeys();
  addr = readline("Enter receiver address: ");
  if(pl_set_receiver_by_address(ctx, atoi(addr)) == 0)
	printf("setting receiver to %s\n",addr);
  else{
  	printf("public key not found :(\n");
    return 0;
  }
  msg = readline("Type your message:\n");
  memcpy(ctx->msg->msg, msg, MSG_SIZE);
  switch(pl_send_message(ctx)) { // send msg
    case 0:
      printf("SEND: OK! \n");
      break;
    case -1:
      printf("SEND: -1 \n");
      break;
    case -2:
      printf("SEND: -2 \n");
      break;
    case -3:
      printf("SEND: -3 \n");
      break;
    default:
      printf("SEND: unknown error \n");
      break;
  }
  pl_send_message_fifo(ctx, "/tmp/pager");
  return 0;
}

int help()
{
  for(int i=0; i < (sizeof(commands)/sizeof(COMMAND)) ; i++)
  {
    printf("%s : %s\n",commands[i].name, commands[i].doc);
  }
}

int quit()
{
  printf("Quitting...\n");
  return 0;
}

int main()
{
  ctx = pl_init();
  pl_load_keypair_from_file(ctx, "tx");
  pl_load_public_keys_from_folder(ctx, "pubkeys");

 
  char *input;                                                                                                                                                                       
  for(;;)
  {
    input = readline("pager-cli: ");
    for(int i=0; i < (sizeof(commands)/sizeof(COMMAND)) ; i++)
    {
      if(strncmp(commands[i].name, input, 20) == 0)
      {
        ((*(commands[i].func)) (input));
        break;
      } 
        //printf("%s: command not found\n",input);
	    //break;
    }
   
  }

  return 0;
}
