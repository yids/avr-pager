//#include "../libs/micro-ecc/uECC.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sodium.h>

#include "../libs/pagerlib/pagerlib.h"
#ifndef uECC_TEST_NUMBER_OF_ITERATIONS
#define uECC_TEST_NUMBER_OF_ITERATIONS   1
#endif

//char shared_secret[SHARED_SECRET_SIZE];
char decompressed_point[64];

void hex_dump(struct pl_keypair *keypair)
{
  printf("private key:\n");
  printf("{");
  for(int i=0; i < crypto_box_SECRETKEYBYTES; i++){ 
    printf("0x%hhx, ", keypair->secret_key[i]);
  }
  printf("}\npublic key:\n{");
  for(int i=0; i < crypto_box_PUBLICKEYBYTES; i++){ 
    printf("0x%hhx, ", keypair->public_key[i]);
  }
  printf("}\n\n");
}


int main()
{
  char filename[64];
  char pubfilename[64];
  struct pl_keypair  *keypair;
  struct pl_ctx * context;
  context = pl_init(context);

  keypair = pl_create_keypair(context);
  printf("enter filename for new keypair:\n");
  scanf("%s", filename);
  pl_save_key(keypair, filename);
  printf("keypair saved in file: %s\n", filename);
  strncat(filename, ".pub", 4);

  pl_save_public_key(keypair->public_key, filename);
  printf("public key saved in file: %s\n", filename);
  
  printf("HEX structs:\n");
  hex_dump(keypair);

  return 0;
}
