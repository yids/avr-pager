#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "../libs/pagerlib/pagerlib.h"

int main ()
{
  char rx_priv_key[crypto_box_SECRETKEYBYTES] = {0x84, 0x70, 0x10, 0x49, 0x01, 0xC3, 0x4F, 0xA2, 0x2A, 0x2A, 0xB9, 0x19, 0x8D, 0x1E, 0x44, 0x46, 0x26, 0x15, 0x3B, 0x92, 0x00, 0x91, 0x6A, 0x41, 0x36, 0xDF, 0x7E, 0x97, 0x6D, 0x3C, 0x6E, 0x01 };
    char rx_comp_p[crypto_box_PUBLICKEYBYTES] = {0xA9, 0xB4, 0xA1, 0xF3, 0x56, 0x27, 0x70, 0x9E, 0xBD, 0x32, 0x27, 0x0A, 0xB8, 0x06, 0xE7, 0xE8, 0x98, 0xA4, 0x11, 0xD8, 0xB5, 0x64, 0x84, 0x64, 0xB6, 0xB6, 0x48, 0xEC, 0x0C, 0xA4, 0x1F, 0x49}; 

  struct pl_ctx *rx_ctx;
  struct pl_keypair *rx_kp;
  rx_kp = NULL;

  /* init */
  rx_ctx = pl_init();

  /*load keypair */
//  pl_load_keypair_from_file(rx_ctx, "rx");
  memcpy(rx_ctx->kp->public_key, rx_comp_p, crypto_box_PUBLICKEYBYTES);
  memcpy(rx_ctx->kp->secret_key, rx_priv_key, crypto_box_SECRETKEYBYTES);

  pl_load_key_in_list(rx_ctx, rx_ctx->kp);
  pl_print_keylist(rx_ctx);

  /* get message from stdin */
  struct pl_pagermessage * message = NULL;
  message = (struct pl_pagermessage *) malloc(sizeof(struct pl_pagermessage));
  read(STDIN_FILENO, message , sizeof(struct pl_pagermessage)); 
  rx_ctx->msg = message;

  message = NULL;

  switch(pl_receive_message(rx_ctx)) {
    case 0:
      printf("RCV: OK! \n");
      break;
    case -1:
      printf("RCV: -1 (box open error)\n");
      break;
    case -2:
      printf("RCV: -2 \n");
      break;
    case -3:
      printf("RCV: -3 (zerobytes not zero)\n");
      break;
    case -4:
      printf("RCV: -4 (key not found) \n");
      break;
    default:
      printf("RCV: unknown error \n");
      break;
  }
 
  pl_inbox_display(rx_ctx);
}
