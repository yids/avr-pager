/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <joerg@FreeBSD.ORG> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.        Joerg Wunsch
 * ----------------------------------------------------------------------------
 *
 * General stdiodemo defines
 *
 * $Id: defines.h 2186 2010-09-22 10:25:15Z aboyapati $
 */

/* HD44780 LCD port connections */

/*
// RW, connected to ground on our thingie, busy bbit?
#define HD44780_RW B, 2

// enable
#define HD44780_E  B, 1

// rs
#define HD44780_RS B, 0

// The data bits have to be not only in ascending order but also consecutive. 
#define HD44780_D4 D, 4

// Whether to read the busy flag, or fall back to
//   worst-time delays. 
#define USE_BUSY_BIT 0
*/

/* HD44780 LCD port connections */
#define HD44780_RW B, 6
#define HD44780_E  B, 5
#define HD44780_RS B, 4
/* The data bits have to be not only in ascending order but also consecutive. */
//#define HD44780_D4 D, 4
#define HD44780_D4 D, 0
/* Whether to read the busy flag, or fall back to
   worst-time delays. */
#define USE_BUSY_BIT 0

