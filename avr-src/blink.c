/* build test for avr, make a led blink */

#include <stdio.h>                                                                                                                                                                   
#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
  DDRB    |= ((1 << DDB5));
  while (1) {
    PORTB ^= ((1 <<  PB5));
    _delay_ms (2718L);  /* waste cycles for 2.71 s */
  }
}
