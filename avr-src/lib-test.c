/* build test for avr, to test if the libs build */                                                                                                                                
#define F_CPU 16000000UL                                                                                                                                                             
#define BAUD 9600


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <avr/io.h>
#include "../libs/pagerlib/pagerlib.h"
#include "../libs/pagerlib/things.h"
#include <util/delay.h>
#include <util/setbaud.h>
/*
void uart_putchar(char c, FILE *stream) {
    if (c == '\n') {
        uart_putchar('\r', stream);
    }
    loop_until_bit_is_set(UCSR0A, UDRE0);
    UDR0 = c;
}
FILE uart_output = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);

char uart_getchar(FILE *stream) {
    loop_until_bit_is_set(UCSR0A, RXC0); 
    return UDR0;
}
FILE uart_input = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);
*/
void uart_putchar(char c) {
  loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
  UDR0 = c;
}
void uart_print(char *string){
  for(int i = 0; string[i] != '\0'; i++) 
    uart_putchar(string[i]);
} 
char uart_getchar(void) {
  loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
  return UDR0;
}
/*
get_message(struct pl_pagermessage * message)
{
  for(int i=0; i<sizeof(struct pl_pagermessage); i++)
    memset(&message[i], uart_getchar(), sizeof(char));

  uart_print("msg received\n");
  return 0;
}
*/
unsigned char rx_sk[crypto_box_SECRETKEYBYTES] = {0x84, 0x70, 0x10, 0x49, 0x01, 0xC3, 0x4F, 0xA2, 0x2A, 0x2A, 0xB9, 0x19, 0x8D, 0x1E, 0x44, 0x46, 0x26, 0x15, 0x3B, 0x92, 0x00, 0x91, 0x6A, 0x41, 0x36, 0xDF, 0x7E, 0x97, 0x6D, 0x3C, 0x6E, 0x01 };
unsigned char rx_pk[crypto_box_PUBLICKEYBYTES] = {0xA9, 0xB4, 0xA1, 0xF3, 0x56, 0x27, 0x70, 0x9E, 0xBD, 0x32, 0x27, 0x0A, 0xB8, 0x06, 0xE7, 0xE8, 0x98, 0xA4, 0x11, 0xD8, 0xB5, 0x64, 0x84, 0x64, 0xB6, 0xB6, 0x48, 0xEC, 0x0C, 0xA4, 0x1F, 0x49};

unsigned char tx_sk[crypto_box_SECRETKEYBYTES] = {0x7, 0x42, 0xc8, 0x29, 0xe6, 0x83, 0x11, 0x69, 0xb4, 0xa6, 0x46, 0x5, 0xa6, 0x7e, 0x5c, 0xe5, 0xe4, 0x2f, 0xb1, 0x74, 0x2, 0xc4, 0xdf, 0xe3, 0xec, 0x69, 0x59, 0xdc, 0x24, 0x60, 0xb0, 0xf2};
unsigned char tx_pk[crypto_box_PUBLICKEYBYTES] = {0xb0, 0xdd, 0xb4, 0x70, 0xad, 0x7a, 0x2d, 0x82, 0x8a, 0xfa, 0xa4, 0x7b, 0xee, 0xa8, 0xf5, 0xe5, 0xc8, 0xf8, 0x54, 0x69, 0xf3, 0xe5, 0xbc, 0x27, 0xde, 0xf4, 0xbf, 0x63, 0x3a, 0xad, 0x4c, 0x23};
	

int main()
{
  uart_init();

  uart_print("main\n\r");

  struct pl_ctx *tx_ctx;
  char clear_message[] = "Blaat blaat, dlaat ";
  tx_ctx = pl_init();
  memcpy(tx_ctx->kp->secret_key, tx_sk, crypto_box_SECRETKEYBYTES);
  memcpy(tx_ctx->kp->public_key, tx_pk, crypto_box_PUBLICKEYBYTES);
  pl_load_key_in_list(tx_ctx, tx_ctx->kp);
  uart_print("loaded keypair \n\r");
  pl_set_receiver(tx_ctx, rx_pk);                                                                                                                                                   
  memcpy(tx_ctx->msg->msg, clear_message, MSG_SIZE);
  uart_print("loaded message and set receiver \n\r");
  //pl_send_message(tx_ctx);
  //uart_print("encrypted the message \n\r");

  

/*
  struct pl_ctx *rx_ctx;
  rx_ctx = pl_init();

  memcpy(rx_ctx->kp->secret_key, rx_priv_key, crypto_box_SECRETKEYBYTES);
  memcpy(rx_ctx->kp->public_key, rx_comp_p, crypto_box_PUBLICKEYBYTES);
  pl_load_key_in_list(rx_ctx, rx_ctx->kp);
  
  struct list_kp * list;
  for(list = rx_ctx->keypairs; list != NULL; list = list->next) {
    uart_print("id ");
    uart_print(list->id);
  } 

  rx_ctx->msg = message;
  pl_receive_message(rx_ctx) ;
*/
  return 0;
}
