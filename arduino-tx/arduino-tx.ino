#include <uECC.h>
#include <AESLib.h>
#include <RadioHead.h>
#include <RH_ASK.h>
#include <RHDatagram.h>
#include <SPI.h> // Not actually used but needed to compile
#include <MemoryFree.h>
#include <string.h>
#include <pagerlib.h>

#define ADDRESS 5
#define NUM_ECC_DIGITS 24 //size of privkey, curvesize in bytes 
#define CURVE uECC_secp192r1()
#define MESSAGE_SIZE 32
#define PAGER_MESSAGE_SIZE 57

//#define MESSAGE_SIZE 64
//#define PAGER_MESSAGE_SIZE 99

uint8_t tx_priv_key[ECC_COMPRESSED_SIZE-1] = {0xB4, 0x15, 0x70, 0x3E, 0xA7, 0x5D, 0x06, 0xF2, 0x33, 0x75, 0x8E, 0xE0, 0x86, 0x1B, 0x73, 0xBE, 0xEC, 0x87, 0x36, 0x0F, 0xF5, 0xE1, 0x79, 0x76, 0x2C, 0x3C, 0x74, 0x69, 0x83, 0x71, 0x07, 0xB5};
uint8_t tx_comp_p[ECC_COMPRESSED_SIZE] = {0x02, 0xA3, 0x91, 0x16, 0x71, 0x1E, 0x7B, 0xB2, 0x51, 0x7F, 0xD4, 0xF4, 0xC7, 0x81, 0x49, 0x75, 0x8B, 0x48, 0x75, 0x59, 0x27, 0xC9, 0x94, 0x3F, 0x59, 0xDD, 0xFF, 0x2E, 0x89, 0xE9, 0xD8, 0x1D, 0x2B };

uint8_t rx_priv_key[ECC_COMPRESSED_SIZE-1] = {0x63, 0x36, 0xDD, 0xF1, 0x1E, 0xC2, 0xA1, 0xCF, 0x35, 0xD1, 0x79, 0x8E, 0xD5, 0xA3, 0x82, 0x1F, 0x86, 0x70, 0x8C, 0x5A, 0x09, 0x0D, 0xBA, 0xC9, 0xC5, 0xC4, 0x00, 0xE5, 0xA5, 0x7F, 0x16, 0xBA};
uint8_t rx_comp_p[ECC_COMPRESSED_SIZE] = {0x03, 0xAF, 0x52, 0x05, 0xBF, 0xC5, 0x74, 0xF8, 0xD5, 0xA9, 0xEC, 0x36, 0x65, 0x1B, 0x8F, 0x4A, 0x73, 0x0B, 0x12, 0x2E, 0x89, 0xA6, 0x9B, 0x5F, 0xA4, 0x18, 0x1E, 0x03, 0x5E, 0x95, 0xB1, 0x5F, 0x9F};
 
struct pl_keypair *sender, *receiver;
char clear_message[] = "dit is een test berichtje :) ";

struct pl_ctx * context;

RH_ASK driver(2000);
RHDatagram manager(driver, ADDRESS);

void sendAll(char* msg)
{
    
    manager.setHeaderId(52);
   // manager.sendto((uint8_t *)pagerMsg, PAGER_MESSAGE_SIZE, 2);
    manager.waitPacketSent();   
}

void setup()
{
  Serial.begin(9600);	  // Debugging only
  Serial.println("TX init");
  if (!manager.init())
       Serial.println("init failed");
  //Serial.print("freeMemory()=");
  //Serial.println(freeMemory());
  context = pl_init();
  sender = (struct pl_keypair *) malloc(sizeof(struct pl_keypair));
  receiver = (struct pl_keypair *) malloc(sizeof(struct pl_keypair));
}

void loop()
{

  Serial.println("start");   
  Serial.print("freeMemory()=");
  Serial.println(freeMemory());
 
  //pl_load_key(sender, "sender.keypair"); // load tx keypair
  //pl_load_key(receiver, "receiver.keypair"); // load tx keypair
  memcpy(sender->private_key, rx_priv_key, ECC_COMPRESSED_SIZE-1);
  memcpy(sender->compressed_point, rx_comp_p, ECC_COMPRESSED_SIZE);

  memcpy(receiver->private_key, tx_priv_key, ECC_COMPRESSED_SIZE-1);
  memcpy(receiver->compressed_point, tx_comp_p, ECC_COMPRESSED_SIZE);

  context->kp = sender;

  memcpy(context->msg->msg, clear_message, MSG_SIZE);
  memcpy(&context->receiver_compressed_point, &receiver->compressed_point, sizeof(context->receiver_compressed_point));
     
  pl_send_message(context);

  uint8_t buffer[sizeof(struct pl_pagermessage)];
  memcpy(context->msg, buffer, sizeof(struct pl_pagermessage));

;

  manager.setHeaderId(52);
  manager.sendto(buffer ,sizeof(struct pl_pagermessage), 2);
  manager.waitPacketSent();


  //receive the message
  context->kp = receiver;
  pl_receive_message(context);
  Serial.println("decrypted msg:");
  Serial.println(context->msg->msg);

//  delay(200);
}
