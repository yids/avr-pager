
#include <stdarg.h>
#include <uECC.h>
#include <AESLib.h>
#include <RadioHead.h>
#include <RH_ASK.h>
#include <RHDatagram.h>
#include <SPI.h> // Not actualy used but needed to compile
#include <string.h>
#include <Wire.h>
//#include <LCDI2C_LK162-12.h>
#include <MemoryFree.h>
#include <pagerlib.h>
#include <LiquidCrystal.h>
#include <avr/pgmspace.h>

/* 
 * pagerlib things
 */
aes_context ctx; // context for the cbc crypto stuff

struct pl_keypair *sender, *receiver, *kp;
char clear_message[] = "dit is een test berichtje :) ";
//const char clear_message[] PROGMEM = "Blaat coblaat, dit is een test berichtje :), en nog meer en meer en meer 123456744555 blablablablablablabal jajajajaj hee blaat ";
//const char b64_key[] PROGMEM = "Zs8rZ5EfIpMs2i5Mskq2Nnx0pHhKShirwswQbIAjXxUDkPCKKSD1o+oIF84qwlUk85B1W0lN4AyEtJZNk24HKL8=";
//const char b64_key2[] PROGMEM = "6+owJHKEhmoz1RI3dx+x7JODIjueBOZUmzjxSrmrpZ8COt4Tld6bsa2UlFnt4qDMf/Rz5M60d/wjgXzafEkCg54=";
const char b64_key[] = "Zs8rZ5EfIpMs2i5Mskq2Nnx0pHhKShirwswQbIAjXxUDkPCKKSD1o+oIF84qwlUk85B1W0lN4AyEtJZNk24HKL8=";
const char b64_key2[]= "6+owJHKEhmoz1RI3dx+x7JODIjueBOZUmzjxSrmrpZ8COt4Tld6bsa2UlFnt4qDMf/Rz5M60d/wjgXzafEkCg54=";




struct pl_ctx * context;

char output[MSG_SIZE] = "outputcharbla" ;

#define LCD
#define RADIO

/*
 * hardware things
 */ 

#define ADDRESS 2
/*
#define MESSAGE_SIZE 32
#define PAGER_MESSAGE_SIZE 57
 */

#ifdef RADIO
RH_ASK driver(2000, 2, 12, 13);
RHDatagram manager(driver, ADDRESS);
#endif
#ifdef LCD
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

// define some values used by the panel and buttons
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5
#endif
void setup()
{
#ifdef LCD
  // select the pins used on the LCD panel
  lcd.begin(16, 2);              // start the library
  lcd.setCursor(0,0);
  // lcd.print("Push the buttons"); // print a simple message
  //pinMode(10, OUTPUT);        
  //digitalWrite(10, HIGH);

  //  lcd.init();
  lcd.clear();
  lcd.print("dit is een pager");
#endif 
  Serial.begin(9600);	// Debugging only

#ifdef RADIO
  Serial.println("RX init");
  if (!manager.init())
    Serial.println("init failed");
#endif

  Serial.print("freeMemory()=");
  Serial.println(freeMemory());
  context = pl_init();
  //  receiver = (struct pl_keypair *) malloc(sizeof(struct pl_keypair));

  // init pager
  //  memcpy(receiver->private_key, tx_priv_key, ECC_COMPRESSED_SIZE-1);
  //  memcpy(receiver->compressed_point, tx_comp_p, ECC_COMPRESSED_SIZE);

#ifdef LCD
  lcd.setCursor(0,1);
  // create receiver keypair and load in list
  lcd.print("*");
#endif

  /*
   * load the keys from the base64 thingies
   */

  receiver = pl_create_keypair(context);
  if (receiver == NULL)
{ 
  Serial.println("creating receiver keypair failed! err:");
  Serial.println(context->err);
}
 if (1) {
    Serial.println("1 is waar");
} 
  //receiver = (struct pl_keypair *)malloc(sizeof(struct pl_keypair));
  //base64_decode((char *)receiver , (char *)&b64_key, (4*sizeof(struct pl_keypair) / 3)) ;
/*
  pl_load_key_in_list(context, receiver);

  // create sender keypair
  sender = pl_create_keypair(context);
  if (sender == NULL)
{ 
  Serial.println("creating sender keypair failed! err:");
  Serial.println(context->err);
}
*/
  //sender = (struct pl_keypair *)malloc(sizeof(struct pl_keypair));
  //base64_decode((char *)sender , (char *)&b64_key2, (4*sizeof(struct pl_keypair) / 3)) ;

  //  pl_load_key_in_list(context, kp);
  // set default
  context->kp = sender;
#ifdef LCD
  lcd.clear();
  lcd.print("finished making keys");
#endif
}
#ifdef LCD
// read the buttons
int read_LCD_buttons()
{
  adc_key_in = analogRead(0);      // read the value from the sensor 
  // my buttons when read are centered at these valies: 0, 144, 329, 504, 741
  // we add approx 50 to those values and check to see if we are close
  if (adc_key_in > 1000) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
  // For V1.1 us this threshold
  if (adc_key_in < 50)   return btnRIGHT;  
  if (adc_key_in < 250)  return btnUP; 
  if (adc_key_in < 450)  return btnDOWN; 
  if (adc_key_in < 650)  return btnLEFT; 
  if (adc_key_in < 850)  return btnSELECT;  

  // For V1.0 comment the other threshold and use the one below:
  /*
     if (adc_key_in < 50)   return btnRIGHT;  
     if (adc_key_in < 195)  return btnUP; 
     if (adc_key_in < 380)  return btnDOWN; 
     if (adc_key_in < 555)  return btnLEFT; 
     if (adc_key_in < 790)  return btnSELECT;   
   */
}
#endif
void display(char *msg)
{
  /*   char line1[16];
       char line2[16];
       for(int i=0; i < MESSAGE_SIZE ; i++){
       if(i < 16)
       line1[i] = msg[i];
       else
       line2[i-16] = msg[i];
       };
   */
  //lcd.clear();
  //lcd.println(msg);
}
/*
   void loop()
   {
   lcd.setCursor(9,1);            // move cursor to second line "1" and 9 spaces over
   lcd.print(millis()/1000);      // display seconds elapsed since power-up


   lcd.setCursor(0,1);            // move to the begining of the second line
   lcd_key = read_LCD_buttons();  // read the buttons

   switch (lcd_key)               // depending on which button was pushed, we perform an action
   {
   case btnRIGHT:
   {
   lcd.print("RIGHT ");
   break;
   }
   case btnLEFT:
   {
   lcd.print("LEFT   ");
   break;
   }
   case btnUP:
   {
   lcd.print("UP    ");
   break;
   }
   case btnDOWN:
   {
   lcd.print("DOWN  ");
   break;
   }
   case btnSELECT:
   {
   lcd.print("SELECT");
   break;
   }
   case btnNONE:
   {
   lcd.print("NONE  ");
   break;
   }
   }

   }
 */

void loop()
{

  Serial.println("bla\n");
#ifdef LCD
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(millis()/100);
#endif
  memcpy(context->msg->msg + 8, "bla", MSG_SIZE - 8);


  memcpy(&context->msg->msg, clear_message, MSG_SIZE);
  //    context->kp = kp;
  //to who to send the message to (get from kp)
  memcpy(&context->receiver_compressed_point, &kp->compressed_point, sizeof(context->receiver_compressed_point));

  //get msg with radio
  //memcpy(context->msg->msg + 8, "bla", MSG_SIZE - 8);
  if( pl_send_message(context) != 0)
  {
    Serial.println("error in pl_send_message: ");
    Serial.println(context->err);
    context->err = 0;
  } else {
    Serial.println("no error");
  }

Serial.println("crypted message addr:");
  Serial.println(context->msg->address);

  Serial.println("crypted message sender_dec_point:");
  Serial.println((char *)context->decompressed_point);

  Serial.println("crypted message sender_comp_point:");
  Serial.println((char *)context->msg->sender_compressed_point);

  Serial.println("crypted message iv:");
  Serial.println((char *)&context->msg->iv);

  Serial.println("crypted message msg:");
  Serial.println((char *)context->msg->msg);

  //  base64_encode((char *)&output, context->msg->msg, MSG_SIZE);
#ifdef LCD
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("crypted message:");
  delay(1000);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(context->msg->msg);
#endif

/*

  if (pl_receive_message(context) != 0)
  {
    Serial.println("error in pl_receiver_message");
    Serial.println(context->err);
    context->err = 0;
  }
*/
#ifdef LCD
  delay(1000);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("decrypted msg:");
  lcd.setCursor(0,0);
  lcd.println(context->msg->msg);
  delay(1000);
  lcd.clear();
#endif
}

