
#ifdef __cplusplus
extern "C"
{
#endif
//#define NOCRYPT
#define DEBUG

#if defined(__AVR_ATmega328P__)
#define ARDUINO
#endif

#ifdef ARDUINO
 #include "../avrnacl/avrnacl.h"
#else
 #include <unistd.h>
 #include <ctype.h>
 #include <sodium.h>
#endif

#include "packets.h"
#include "things.h"

#ifdef DEBUG
 #ifdef ARDUINO // fixme arduino debug functions
  #define DBG
  #define DB
  #define DBM
 #else
 #define DBG(f_, ...) printf((f_), ##__VA_ARGS__);
 #define DB(f_, ...) dump_buffer((f_), ##__VA_ARGS__);
 #define DBM(f_, ...) dump_buffer_msg((f_), ##__VA_ARGS__);
 #endif
#else // no debuging
 #define DBG
 #define DB
 #define DBM
#endif

//typedef enum { false, true } bool;

/* 
 * List of all the available keypairs
 * Should we use same list for only pubkeys? 
 */
typedef struct list_kp {
  uint32_t id;
  struct pl_keypair *kp;
  struct list_kp *next;
  // things maybe:
  // - nickname
  // - message counter
} list_kp;

/*
 * List of all public keys, their addresses, names 
 * 
 */
typedef struct list_public_key {
  uint32_t id;
  unsigned char public_key[crypto_box_PUBLICKEYBYTES];
  //bool is_group;
  char alias[32]; 
  struct list_public_key *prev;
  struct list_public_key *next;
} list_public_key;


/* 
 * the inbox
 */
#define MSG_NEW 1
#define MSG_READ 2
typedef struct list_inbox {
  int status; // 1 unread 2 read 
  int id; 
  struct pl_pagermessage *msg;
  struct list_inbox *prev;
  struct list_inbox *next;
} list_inbox;

/*
 * pagerlib context for sending or receiving messages
 */
struct pl_ctx
{
  // contains the message that will be send or is received
 // char clear_message[MSG_SIZE];
  char decryptbuffer[PLAIN_MSG_SIZE];
  struct pl_keypair *kp; // my (currently used ) keypair
  struct list_kp *keypairs;  // keypair list (all available keypairs)
  struct pl_pagermessage *msg;  // place to store messages to be sent or received
  int msgcount;
  struct list_inbox *inbox;
  struct list_inbox *inbox_curr;
#ifndef RX
  unsigned char receiver_public_key[crypto_box_PUBLICKEYBYTES];
  struct list_public_key *public_keys;
#endif
  uint8_t err;
};

struct pl_ctx *  pl_init();
struct pl_keypair * pl_create_keypair(struct pl_ctx*);

int pl_set_receiver(struct pl_ctx *ctx, unsigned char *public_key);
int pl_set_receiver_by_address(struct pl_ctx *ctx, uint32_t addr);

int pl_send_message(struct pl_ctx *);
int pl_send_message_fifo(struct pl_ctx *ctx, char *fifo);
int pl_receive_message(struct pl_ctx*);

int pl_save_key(struct pl_keypair *key, char * filename);
int pl_load_keypair_from_file(struct pl_ctx *ctx, char *filename);
int pl_load_keypair(struct pl_ctx *ctx, struct pl_keypair *kp);
int pl_save_public_key(char pubkey[crypto_box_PUBLICKEYBYTES], char *filename);
char *pl_load_public_key(char *filename);
int pl_print_keylist(struct pl_ctx *ctx);
int pl_load_key_in_list(struct pl_ctx *ctx, struct pl_keypair *key);
int pl_load_public_key_in_list(struct pl_ctx *ctx, unsigned char *public_key, char *alias);
int pl_print_public_keylist(struct pl_ctx *ctx);
int pl_load_public_keys_from_folder(struct pl_ctx *ctx, char const *folder);

int pl_inbox_append(struct pl_ctx *ctx, struct pl_pagermessage *msg);
uint8_t compressed_point_to_addr( uint8_t input[]);
int pl_inbox_display(struct pl_ctx *ctx);
int pl_inbox_next(struct pl_ctx *ctx);
int pl_inbox_prev(struct pl_ctx *ctx);
int pl_inbox_delete(struct pl_ctx *ctx);
int pl_cleanup(struct pl_ctx *ctx);
int is_zero( const char *data, int len );

#ifdef __cplusplus
} /* end of extern "C" */
#endif




