#ifndef ARDUINO
  #include <fcntl.h>
  #include <dirent.h>
  #include <sys/stat.h>
  #include <sys/types.h>

#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "packets.h"
#include <stdlib.h>
#include "things.h"
#include "pagerlib.h"

#ifndef ARDUINO
//#include "nacl/crypto_box.h"
#else
//#include "avrnacl.h"
#endif


/*
* int rng (uint8_t *dest, unsigned size)
* random number generator
* uint8_t *dest destination for the random numbers
* unsigned size number of random bytes to put in dest
*/
int rng (uint8_t *dest, unsigned size) 
{
#ifndef ARDUINO
  int fd = open("/dev/urandom", O_RDONLY);
  if (fd == -1) {
    fd = open("/dev/random", O_RDONLY);
    if (fd == -1) {
      return 0;
    }
  }

  char *ptr = (char *)dest;
  size_t left = size;
  while (left > 0) {
    size_t bytes_read = read(fd, ptr, left);
    if (bytes_read <= 0) { // read failed
      close(fd);
      return 0;
    }
    left -= bytes_read;
    ptr += bytes_read;
  }

  close(fd);
  return 1;
#else
  // arduino rng comes here
  //memcpy(dest, &fakeiv, size);
  return 0;
#endif
}

/* 
* takes a compressed point and turns it into an address
* FIXME: should become a proper hash function 
*/
uint8_t compressed_point_to_addr( uint8_t input[])
{
  uint8_t hashval;
  int i = 0;
  //  DBM("hash address input", ECC_COMPRESSED_SIZE, input);
  while( hashval < UINT32_MAX && i < sizeof(input) ) {
    //printf("hashval %u  i: %u\n ", hashval, i);
    hashval = hashval << 8;
    hashval += input[ i ];
    i++;
  }
  return hashval;
}

/*
* inline struct pl_ctx * pl_init()
* allocate memory for all the stuff in pl_ctx
* returns a pointer to pl_ctx
*/
inline struct pl_ctx * pl_init() 
{
  struct pl_ctx * ctx;
  ctx = (struct pl_ctx *) malloc(sizeof(struct pl_ctx));
  ctx->msg = (struct pl_pagermessage *) malloc(sizeof(struct pl_pagermessage));
//  memset(ctx->msg, 7, sizeof(struct pl_pagermessage)); this is for testing no?//
  ctx->kp = (struct pl_keypair *) malloc(sizeof(struct pl_keypair));; // why NULL? 
  ctx->keypairs = NULL;
  ctx->inbox = NULL;
  return ctx;
}

#ifndef RX  // al these functions are not needed for receiving messages
inline int pl_set_receiver(struct pl_ctx *ctx, unsigned char *public_key)
{
  memcpy(ctx->receiver_public_key, public_key, crypto_box_PUBLICKEYBYTES); 
  return 0; 
}

/* int pl_set_receiver_by_address(struct pl_ctx *ctx, uint32_t addr);
 * loops over the list of public keys and sets the key with address addr as the receiver in the context
 * returns 0 if key is found
 * returns 1 if key is not found
*/
int pl_set_receiver_by_address(struct pl_ctx *ctx, uint32_t addr)
{
  struct list_public_key * list;
  int found = 0;
  for(list = ctx->public_keys; list != NULL; list = list->next) {
      if (addr == list->id){
        // set keypair to use
        memcpy(ctx->receiver_public_key, list->public_key, crypto_box_PUBLICKEYBYTES);
        found = 1;
        return 0;
      }
  }
  return 1;
}

/*
 * This sends a message (encrypts it)
 * Before calling this make sure u set sender ( ctx->kp , ctx->msg->sender_compressed_point) and receiver ctx->receiver_compressed_point, and the message you want to send in ctx->msg->msg
 */

inline int pl_send_message(struct pl_ctx *ctx) {

  // copy sender public key from keypair into pager message
  memcpy(ctx->msg->sender_public_key, ctx->kp->public_key, crypto_box_PUBLICKEYBYTES);

  // turn receiver public key into address
  ctx->msg->address = compressed_point_to_addr(ctx->receiver_public_key);

  // encrypt with NaCl
  unsigned char temp_plain[PLAIN_MSG_SIZE];
  unsigned char temp_encrypted[CRYPTED_MSG_SIZE];
  int rc;

//  if(MSG_SIZE+crypto_box_ZEROBYTES >= MAX_MSG_SIZE) {
//    return -2;
//  }
  memset(temp_plain, '\0', crypto_box_ZEROBYTES);
  memcpy(temp_plain + crypto_box_ZEROBYTES, ctx->msg->msg, PLAIN_MSG_SIZE); 


//char nonce[crypto_box_NONCEBYTES];
#ifdef ARDUINO
  memset(ctx->msg->nonce, '\0', crypto_box_NONCEBYTES); //FIXME this is for testing purposes and should be changed
  uart_write("nonce crap\n\r");
#else
//  randombytes(ctx->msg->nonce, crypto_box_NONCEBYTES);
#endif
  rc = crypto_box(temp_encrypted, temp_plain, crypto_box_ZEROBYTES + PLAIN_MSG_SIZE, ctx->msg->nonce, ctx->receiver_public_key, ctx->kp->secret_key);
 

//  if( rc != 0 ) 
//    return -1;
  //if( is_zero(temp_plain, crypto_box_BOXZEROBYTES) != 0 ) 
  //  return -3;
  
  memcpy(ctx->msg->msg, temp_encrypted, CRYPTED_MSG_SIZE);


//  printf("temp_encrypted: \n %s \n ", temp_encrypted);
//  return crypto_box_ZEROBYTES + length - crypto_box_BOXZEROBYTES;

  return 0;
}

#ifndef ARDUINO
/* pl_send_message_fifo(struct pl_ctx * ctx, char *fifo)
 * sends the pagermessage to a fifo
*/
int pl_send_message_fifo(struct pl_ctx *ctx, char *fifo)
{
  int fd;
  mkfifo(fifo, 0666);
  fd = open(fifo, O_WRONLY);
  write(fd, ctx->msg,  sizeof(struct pl_pagermessage));
  close(fd);
  //unlink(fifo);
  return 0;
} 
#endif
#endif  //this is the endif for the ifndef RX

int pl_receive_message(struct pl_ctx * ctx)
{
//  memset(&ctx->clear_message, 7, sizeof(ctx->clear_message));

//  DBM("msg->msg in pl_receive_message before crypt", sizeof(ctx->msg->msg), &ctx->msg->msg);

 /* check if message is for us */ 
  struct list_kp * list;
  int found = 0;
  for(list = ctx->keypairs; list != NULL; list = list->next) {
#ifndef ARDUINO
    printf("\n address in msg: %u address in list: %u \n", ctx->msg->address, list->id );
#endif
      if (ctx->msg->address == list->id){
        // set keypair to use
        ctx->kp = list->kp;
        found = 1;
        break;
      }
  }
  // exit (and trow away msg) when address not found
/*  if (found == 0) {
    ctx->msg = (struct pl_pagermessage *) malloc(sizeof(struct pl_pagermessage));  
    free(ctx->msg);
    return -4;
  }*/
  pl_inbox_append(ctx, ctx->msg);
  // make pointer to current message null to prevent overwriting the stored message
  ctx->msg = NULL;
  // make space for the next one
  ctx->msg = (struct pl_pagermessage *) malloc(sizeof(struct pl_pagermessage));
  return 0;
}

//////////////////////////////////////////////////////////////
//  pl_decrypt: actually decrypt the message with NaCl     //
////////////////////////////////////////////////////////////
/*
 * Decyrpts the message. 
 * FIXME is it not an idea to have this function return the decrypted message and not store it anywere?
*/

int pl_decrypt(struct pl_ctx * ctx,  struct pl_pagermessage * msg, char plain[PLAIN_MSG_SIZE])
{

  // TODO: figure out why all the hassle with the zerobytes
#ifndef NOCRYPT
  unsigned char temp_encrypted[CRYPTED_MSG_SIZE];
  unsigned char temp_plain[PLAIN_MSG_SIZE];
  int rc;

  memset(temp_encrypted, '\0', crypto_box_BOXZEROBYTES);
  memcpy(temp_encrypted, msg->msg, CRYPTED_MSG_SIZE);

  rc = crypto_box_open(temp_plain, temp_encrypted,  CRYPTED_MSG_SIZE,  msg->nonce, msg->sender_public_key, ctx->kp->secret_key);
    
  if( rc != 0 ) 
    return -1;
  //if( is_zero(temp_plain, crypto_box_ZEROBYTES) != 0 ) 
  //  return -3;

  memcpy(plain, temp_plain + crypto_box_ZEROBYTES, PLAIN_MSG_SIZE);

  // for now store unencrypted in place of encrypted, but should be stored encrypted no? 
//  memcpy(ctx->msg->msg + crypto_box_ZEROBYTES, temp_plain + crypto_box_ZEROBYTES, PLAIN_MSG_SIZE);
   memcpy(ctx->msg->msg, plain, PLAIN_MSG_SIZE);

#ifndef ARDUINO
  printf("new message: \n %s \n ", plain);
#endif
//    return crypto_box_BOXZEROBYTES + MSG_SIZE - crypto_box_ZEROBYTES;
  return 0;
#endif
}


int is_zero( const char *data, int len )
{
  int i;
  int rc;
  rc = 0;
  for(i = 0; i < len; ++i) {
    rc |= data[i];
  }
  return rc;
}

struct pl_keypair * pl_create_keypair(struct pl_ctx *ctx) 
{
  struct pl_keypair *keypair;  
  // arduino doesn't have RNG, so can't be used for generating keys
#ifdef ARDUINO
  return NULL;
#else
  keypair = (struct pl_keypair *) malloc(sizeof(struct pl_keypair));
  crypto_box_keypair(keypair->public_key, keypair->secret_key);
  return keypair;
#endif
}

int pl_save_key(struct pl_keypair *key, char *filename) 
{
#ifdef ARDUINO
  // not implemented
  return 1;
#else
  FILE  *sf;
  sf = fopen(filename, "w");
  fwrite(key, 1, sizeof(struct pl_keypair), sf);
  fclose(sf);
  return 0;
#endif
}

int pl_load_keypair_from_file(struct pl_ctx *ctx, char *filename) 
{
  struct pl_keypair *keypair;
#ifdef ARDUINO
  return 1;
#else
  keypair = (struct pl_keypair *) malloc(sizeof(struct pl_keypair));
  FILE  *lf;
  lf = fopen(filename, "r");
  fread(keypair, 1, sizeof(struct pl_keypair), lf);
  fclose(lf);
  memcpy(ctx->kp, keypair, sizeof(struct pl_keypair));
  free(keypair);
  return 0;
#endif
}

int pl_load_keypair(struct pl_ctx *ctx, struct pl_keypair *kp)
{
  memcpy(ctx->kp, kp, sizeof(struct pl_keypair));
  return 0;
}

int pl_save_public_key(char *pubkey, char *filename)
{
#ifdef ARDUINO
  return 1;
#else
  FILE  *sf;
  sf = fopen(filename, "w");
  fwrite(pubkey, 1, crypto_box_PUBLICKEYBYTES , sf);
  fclose(sf);
  return 0;
#endif
}

/* 
* Load a public key from file, returns the publix key.
*/ 
char * pl_load_public_key(char *filename)
{
#ifdef ARDUINO
  return "1";
#else
  char *public_key;
  public_key = malloc(crypto_box_PUBLICKEYBYTES); 
  FILE  *lf;
  lf = fopen(filename, "r");
  fread(public_key, 1, crypto_box_PUBLICKEYBYTES, lf);
  fclose(lf);
  return public_key;
#endif
}

#ifndef ARDUINO
int pl_load_public_keys_from_folder(struct pl_ctx *ctx, char const *folder)
{
  printf("opening pubkey folder...\n");
  struct dirent *dp;
  DIR *dfd;
  char *dir ;
  if ((dfd = opendir(folder)) == NULL)
  {
    fprintf(stderr, "Can't open %s\n", dir);
    return 0;
  }

  char filename_qfd[100] ;
  char new_name_qfd[100] ;

  while ((dp = readdir(dfd)) != NULL)
  {
    struct stat stbuf ;
 
    sprintf( filename_qfd , "%s/%s",folder,dp->d_name) ;
    stat(filename_qfd, &stbuf);

    if(S_ISREG(stbuf.st_mode)){
      printf("loading key: %s\n", filename_qfd);
      pl_load_public_key_in_list(ctx, pl_load_public_key(filename_qfd), filename_qfd);
    }
       
    if( stat(filename_qfd,&stbuf ) == -1 )
    {
      printf("Unable to stat file: %s\n",filename_qfd) ;
      continue ;
    }
  }
}
#endif

int pl_load_key_in_list(struct pl_ctx *ctx, struct pl_keypair *key)
{
  struct list_kp *list, *ni, *last;
  list = ctx->keypairs;

// make new list item
  ni = (struct list_kp *) malloc( sizeof(struct list_kp) ); 
  ni->next = NULL;
  ni->id = compressed_point_to_addr(key->public_key);
  ni->kp = key;

  // check if list exists
  if (list == NULL) {
//    last = ni;
//    list = ni;
    ctx->keypairs = ni;
  }
  else {
    // walk to end of list
    for(1; 1; list = list->next) {
//      printf("walk: %u \n ", list->next);
      if (list->next == NULL) 
	    break;
    }
    list->next = ni;
  }
}

int pl_print_keylist(struct pl_ctx *ctx) 
{
  struct list_kp * list;
#ifndef ARDUINO
  printf("the list of loaded keys: \n ");
#endif
  for(list = ctx->keypairs; list != NULL; list = list->next) {
#ifndef ARDUINO
    printf("address in list: %u \n", list->id );
#endif
  }
}

int pl_load_public_key_in_list(struct pl_ctx *ctx, unsigned char *public_key, char *alias)
{
  struct list_public_key *list, *ni, *last, *listprev;
  list = ctx->public_keys;
  ni = (struct list_public_key *) malloc( sizeof(struct list_public_key));
  ni->id = compressed_point_to_addr(public_key); 
  ni->next = NULL;
  memcpy(ni->public_key, public_key, crypto_box_PUBLICKEYBYTES); 
  memcpy(ni->alias, alias, 32);
  if (list == NULL) {
    ctx->public_keys = ni;
  }
  else{
    // walk to end of list
    while ( 1 )
    {
      listprev = list;
      if (list->next != NULL) { 
        list = list->next;
        //printf ("AP: middle item (list): %u listprev: %u \n", list->id, listprev->id);
      } 
      else{
        // printf ("AP: last item (list): %u listprev: %u \n", list->id, listprev->id);
        ni->prev = listprev;
        list->next = ni;
        break;
      }
    }
  }
}

int pl_print_public_keylist(struct pl_ctx *ctx) 
{
  struct list_public_key * list;
#ifndef ARDUINO
  printf("currently loaded public keys: \n");
#endif
  for(list = ctx->public_keys; list != NULL; list = list->next) {
#ifndef ARDUINO
    printf("address:%u - alias: %s \n", list->id, list->alias);
#endif
  }
 return 0;
}


int pl_inbox_append(struct pl_ctx *ctx, struct pl_pagermessage *msg)
{
  struct list_inbox *list, *ni, *last, *listprev;
  list = ctx->inbox;
  ctx->msgcount++;
#ifndef ARDUINO
  printf("AP: count: %u", ctx->msgcount);
#endif
  // make new list item
  ni = (struct list_inbox *) malloc( sizeof(struct list_inbox) ); 
  ni->id = ctx->msgcount; // whut?
  ni->next = NULL;
  ni->status = MSG_NEW;
  ni->msg = msg;

  // check if list exists
  if (list == NULL) {
    ctx->inbox = ni;
    ctx->inbox_curr = ni;
  }
  else{
    // walk to end of list
    while ( 1 )
    {
      listprev = list;
      if (list->next != NULL) { 
        list = list->next;
        //printf ("AP: middle item (list): %u listprev: %u \n", list->id, listprev->id);
      } 
      else{
        // printf ("AP: last item (list): %u listprev: %u \n", list->id, listprev->id);
        ni->prev = listprev;
        list->next = ni;
        break;
      }
    }
  }
  return 1;
}

inline int pl_inbox_next(struct pl_ctx *ctx)
{
  if ( ctx->inbox_curr != NULL && ctx->inbox_curr->next != NULL){
    ctx->inbox_curr = ctx->inbox_curr->next;
    return 1;
  }
  else {
#ifndef ARDUINO
    printf("NEXT: already at last item \n");
#endif
    return 0;
  }
}

int pl_inbox_prev(struct pl_ctx *ctx)
{
  if (ctx->inbox_curr != NULL && ctx->inbox_curr->prev != NULL){
    ctx->inbox_curr = ctx->inbox_curr->prev;
    return 1;
  }
  else {
#ifndef ARDUINO
    printf("PREV: already at first item \n");
#endif
    return 0;
  }
}

/*
 * delete currently selected message
 */
int pl_inbox_delete(struct pl_ctx *ctx)
{
  if (ctx->inbox_curr == NULL | ctx->inbox == NULL) 
    return 1;  // make a inbox

  struct list_inbox * dm;
  dm = ctx->inbox_curr;

  if (dm->next != NULL && dm->prev != NULL){ // there both others
    // link prev and next with each other
    dm->next->prev = dm->prev;
    dm->prev->next = dm->next;
  }
  else if (dm->next == NULL && dm->prev == NULL){
    // no others
    ctx->inbox = NULL;
    ctx->inbox_curr = NULL;
  }
  else if (dm->next == NULL && dm->prev != NULL){
    dm->prev->next = NULL;
  } 
  else if (dm->next != NULL && dm->prev == NULL){
    dm->next->prev = NULL;
  }
  free(dm->msg);
  free(dm);
  return 0;
}

int panic(struct pl_ctx *ctx)
{
  // clean decryptbuffer
  //

  // delete all keys
  return -1;
}

/*
write_screen(struct pl_ctx *ctx)
{

}
*/

int pl_inbox_display(struct pl_ctx *ctx) 
{
  char decryptbuffer[100];
  if (ctx->inbox_curr != NULL) {
    struct list_inbox * list;
    list = ctx->inbox_curr;
    pl_decrypt(ctx,list->msg,decryptbuffer);

#ifndef ARDUINO
    printf("m %u | from: %u to: %u  %s \n", list->id , compressed_point_to_addr(list->msg->sender_public_key), list->msg->address, ctx->decryptbuffer );
#endif
    return 0;
  }
}

int pl_inbox_display_all(struct pl_ctx *ctx) 
{
  struct list_inbox * list;
  int i = 0;
#ifndef ARDUINO
  printf("messages: \n ");
#endif
  for(list = ctx->inbox; list != NULL; list = list->next) {
#ifndef ARDUINO
    printf("m %u | from: %u to: %u  %s \n", list->id , compressed_point_to_addr(list->msg->sender_public_key), list->msg->address, list->msg->msg );
#endif
    i++; // <- this doesnt do anything does it?
  }
}

int pl_cleanup(struct pl_ctx *ctx)
{
#ifndef ARDUINO
  struct list_kp * list_kp;
  struct list_public_key * list_pk;
  struct list_inbox * list_inbox;
  printf("cleaning up...\n");
  for(list_kp = ctx->keypairs; list_kp != NULL; list_kp = list_kp->next)
	free(list_kp->kp);
  for(list_pk = ctx->public_keys; list_pk != NULL; list_pk = list_pk->next)  
	free(list_pk->public_key);	
  for(list_inbox = ctx->inbox; list_inbox != NULL; list_inbox = list_inbox->next)  
    free(list_inbox->msg); 
  free(ctx->kp);
  free(ctx->msg);
  //free(ctx->receiver_public_key);
  free(ctx->keypairs);
  free(ctx->public_keys);
  free(ctx->inbox); 
  free(ctx);
  return 0;
#endif
}
