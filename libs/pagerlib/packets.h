
#ifndef packets_h


#ifdef __cplusplus
extern "C"
{
#endif

#ifndef ARDUINO
  #include <sodium.h>
#else
  #include "../avrnacl/avrnacl.h"
#endif

// the text message
#define PLAIN_MSG_SIZE 80
#define CRYPTED_MSG_SIZE PLAIN_MSG_SIZE + crypto_box_ZEROBYTES
#define MSG_SIZE CRYPTED_MSG_SIZE
  
  // the address (hash or part of pubkey?)
#define ADDRESS_SIZE 2

struct pl_keypair
{
  unsigned char public_key[crypto_box_PUBLICKEYBYTES];
  unsigned char secret_key[crypto_box_SECRETKEYBYTES];
};

struct pl_pagermessage 
{
  uint8_t address;
  unsigned char sender_public_key[crypto_box_PUBLICKEYBYTES];
  unsigned char nonce[crypto_box_NONCEBYTES];
  unsigned char msg[MSG_SIZE];
};


/* lorawan
Rate    Max payload size
0       51
1       51
2       51
3       115
4       222
5       222
6       222
7       222
8:15    not defined
*/

#ifdef __cplusplus
} /* end of extern "C" */
#endif

#endif

#define packets_h

